using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace Server
{
    public class ClientObject: IDisposable
    {
        
        
        protected internal string Id { get; private set; }
        protected internal NetworkStream Stream {get; private set;}
        string userName;
        TcpClient client;
        ServerObject server; // объект сервера
        private bool _disposed = false;
        public List<ClientObject> clients = new List<ClientObject>(); // все подключения
 
        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            server = serverObject;
           // serverObject.AddConnection(this);
        }

        private void SendMessageFromServer(string message, List<ClientObject> clients)
        {
            server.BroadcastMessage(new Message(message), this.Id, clients);
            Console.WriteLine(message);
        }
 
        public void Process()
        {
            try
            {
                Stream = client.GetStream();
                // получаем имя пользователя
                string message = GetMessage();
                userName = message;
 
                message = userName + " вошел в чат";
                // посылаем сообщение о входе в чат всем подключенным пользователям
                SendMessageFromServer(message, clients);
                
                // в бесконечном цикле получаем сообщения от клиента
                while (true)
                {
                    try
                    {
                        message = GetMessage();
                        message = String.Format("{0}: {1}", userName, message);
                        SendMessageFromServer(message, clients);
                    }
                    catch
                    {
                        message = String.Format("{0}: покинул чат", userName);
                        SendMessageFromServer(message, clients);
                        break;
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                // в случае выхода из цикла закрываем ресурсы
                server.RemoveConnection(this.Id, clients);
                Close();
            }
        }
 
        // чтение входящего сообщения и преобразование в строку
        private string GetMessage()
        {
            byte[] data = new byte[64]; // буфер для получаемых данных
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = Stream.Read(data, 0, data.Length);
                builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
            }
            while (Stream.DataAvailable);
 
            return builder.ToString();
        }
 
        // закрытие подключения
        protected internal void Close()
        {
            if (Stream != null)
                Stream.Close();
            if (client != null)
                client.Close();
        }

        ~ClientObject()
        {
            Dispose(false);
        }
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }
        private void Dispose(bool disposer)
        {
            if (disposer)
            {
                Id = String.Empty;
                Stream = null;
                userName = String.Empty;
                client = null;
                server = null;
            }
            _disposed = true;
        }
    }
}