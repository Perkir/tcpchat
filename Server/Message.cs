using System;
using System.Runtime.Serialization;

namespace Server
{
    [Serializable]
    public class Message
    {
        [OptionalField] public string Code;
        [OptionalField] public int ErrorText;
        private DateTime _time;
        private string _text;

        public Message(string msg)
        {
            _text = msg;
            _time = DateTime.Now;
        }
    }
}