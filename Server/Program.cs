﻿using System;
using System.Threading;

namespace Server
{
    internal class Program
    {
        // private static ServerObject _server; // сервер
        // private static Thread _listenThread; // потока для прослушивания

        private static void Main(string[] args)
        {
            using ServerObject _server = new ServerObject();
            Thread _listenThread = new Thread(new ThreadStart(_server.Listen));
            _listenThread.Start(); //старт потока
            
        }
        //_server.Disconnect();
    }
}