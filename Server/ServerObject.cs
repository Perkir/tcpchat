using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Runtime.Serialization.Json;

namespace Server
{
    public class ServerObject: IDisposable
    {
         TcpListener tcpListener; // сервер для прослушивания
         public List<ClientObject> clients = new List<ClientObject>(); // все подключения
         private bool _disposed = false;

        public void AddConnection(ClientObject clientObject, List<ClientObject> clients)
        {
            clients.Add(clientObject);
            Console.WriteLine("Добавлен клиент: " + clientObject.Id);
        }
        protected internal void RemoveConnection(string id, List<ClientObject> clients)
        {
            // получаем по id закрытое подключение
            ClientObject client = clients.FirstOrDefault(c => c.Id == id);
            // и удаляем его из списка подключений
            if (client != null)
                clients.Remove(client);
            Console.WriteLine("Удален клиент: " + client.Id);
        }
        // прослушивание входящих подключений
        protected internal void Listen()
        {
            try
            {
                tcpListener = new TcpListener(IPAddress.Any, 8888);
                tcpListener.Start();
                Console.WriteLine("Сервер запущен. Ожидание подключений...");
 
                while (true)
                {
                    List<ClientObject> clients = new List<ClientObject>();
                    TcpClient tcpClient = tcpListener.AcceptTcpClient();
                    ClientObject clientObject = new ClientObject(tcpClient, this);
                    this.AddConnection(clientObject, clients);
                    Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
                    clientThread.Start();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                //Disconnect();
            }
        }
 
        // трансляция сообщения подключенным клиентам
        protected internal void BroadcastMessage(Message msg, string id, List<ClientObject> clients)
        {
            //var formatter = new BinaryFormatter();
            var jsonFormatter = new DataContractJsonSerializer(typeof(Message));
            foreach (var t in clients.Where(t => t.Id!= id))
            {
                jsonFormatter.WriteObject(t.Stream, msg); //передача данных
            }
        }
        // отключение всех клиентов
        protected internal void Disconnect()
        {
            tcpListener.Stop(); //остановка сервера
 
            foreach (var t in clients)
            {
                t.Close(); //отключение клиента
            }
            Environment.Exit(0); //завершение процесса
        }
        ~ServerObject()
        {
            Dispose(false);
        }
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }
        private void Dispose(bool disposer)
        {
            if (disposer)
            {
                tcpListener = null;
                clients = null;
            }
            _disposed = true;
        }
    }
}